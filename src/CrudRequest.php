<?php

namespace Grimzy\LaravelCrud;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\ParameterBag;

class CrudRequest extends FormRequest
{
    /**
     * Processed fields (after validation)
     *
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    public $fields;

    protected $crud;

    public function __construct(
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null,
        Crud $crud
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->crud = $crud;
        $this->fields = new ParameterBag();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation_array = [];
        $fields = $this->crud->getFields();
        if ($fields) {
            foreach ($fields as $field_name => $field) {
                $rules = $field->getValidation();
                if (is_string($rules)) {
                    $rules = [$field_name => $rules];
                }

                if (is_array($rules)) {
                    $validation_array = array_merge($validation_array, $rules);
                }
            }
        }

        return $validation_array;
    }

    public function validate()
    {
        parent::validate();

        // Post-process the fields after validation
        $validated_fields = $this->crud->getFields();
        $input = $this->all();

        foreach($validated_fields as $field_name => $field) {
            $value = $field->transformValueFromInput($input);
            $this->fields->set($field_name, $value);
        }
    }

    public function all()
    {
        return array_replace_recursive($this->input(), $this->allFiles(), $this->fields->all());
    }
}
