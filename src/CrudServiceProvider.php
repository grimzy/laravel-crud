<?php
namespace Grimzy\LaravelCrud;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Config
        $path = __DIR__ . '/../config/crud.php';
        $this->publishes([$path => config_path('crud.php')], 'crud_config');
        $this->mergeConfigFrom($path, 'crud');

        $omitParenthesis = version_compare($this->app->version(), '5.3', '<');
        Blade::directive('field', function ($expression) use ($omitParenthesis) {
            $expression = $omitParenthesis ? $expression : "($expression)";

            return "<?php echo app('crud')->displayField{$expression}; ?>";
        });

        Blade::directive('fields', function ($expression) use ($omitParenthesis) {
            $expression = $omitParenthesis ? $expression : "($expression)";

            return "<?php echo app('crud')->displayFields{$expression}; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('crud', function ($app) {
            return new Crud($app->make('request'));
        });

        //$this->app->bind('crud', function () {
        //    return new Crud($this->app->make('request'));
        //});
    }
}