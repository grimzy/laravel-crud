<?php

namespace Grimzy\LaravelCrud;

abstract class AbstractField    // TODO: Split in interfaces and use this as a helper?
{
    public $name;

    protected $resource;

    protected $model;

    /**
     * @var array|null
     */
    public $relation;

    /**
     * @var array|null|string
     */
    protected $validation;

    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Constructor.
     *
     * @param string $name
     * @param string $resource
     * @param string $model
     * @param array|null $relation
     * @param array|null|string $validation
     * @param array $config
     */
    public function __construct($name, $resource, $model, $relation, $validation = null, array $config = [])
    {
        $this->name = $name;
        $this->relation = $relation;
        $this->setValidation($validation);

        // TODO: try to avoid having to do this and instead have those in the views directly.
        $this->resource = $resource;
        $this->model = $model;

        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }
    }

    /**
     * @param array|null|string $validation
     */
    public function setValidation($validation)
    {
        $this->validation = $validation;
    }

    /**
     * @return array|null|string
     */
    public function getValidation()
    {
        return $this->validation;
    }

    protected abstract function getTemplate();

    // TODO: make this abstract or try to make this take containers like Crud and so on
    public function display($result)
    {

        return view('crud.fields.'.$this->getTemplate(), [
            'item' => $result,
            'field' => $this->name,
            'resource' => $this->resource,
            'model' => $this->model,
        ]);
    }

    /**
     * @param array $input
     * @return mixed|null
     */
    public function transformValueFromInput(array $input = [])
    {
        return ! empty($input[$this->name]) ? $input[$this->name] : null;
    }
}