<?php

namespace Grimzy\LaravelCrud;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class Crud
{
    private $config_directory;

    private $fields_namespace;

    private $views_directory;

    protected $request;

    protected $controller;

    protected $model;

    protected $action;

    /**
     * @var string|null
     */
    protected $resource;

    /**
     * @var \Grimzy\LaravelCrud\AbstractField[]
     */
    protected $fields;

    protected static $results;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->config_directory = config('crud.config_directory');
        $this->fields_namespace = config('crud.fields_namespace');
        $this->views_directory = config('crud.views_directory');

        if ($route = $request->route()) {
            $action = $route->getAction();
            list($this->controller, $this->action) = explode('@', $action['controller']);

            $class = $this->controller;
            if (is_subclass_of($class, 'Grimzy\LaravelCrud\CrudController')) {
                $this->model = $class::$model;
                $this->resource = $class::$resource;
                $this->instantiateFields();
            }
        }
    }

    public function displayFields()
    {
        $content = '';

        foreach ($this->getFields() as $field) {
            $content .= $this->displayField($field->name);
        }

        return $content;
    }

    public function displayField($field_name, $item = null)
    {
        $content = '';

        if (! $item) {
            $item = $this::$results;
        }

        $display = $this->fields[$field_name]->display($item);
        if (! empty($display) && in_array(Renderable::class, class_implements($display))) {
            $content .= $display->render();
        } else {
            $content .= $display;
        }

        return $content;
    }

    protected function instantiateFields()
    {
        $all_fields_config = $this->getAllFieldsConfig();
        $fields_config = $this->getFieldsConfigForAction();

        foreach ($fields_config as $field_name => $config) {
            // Add the field
            $class_name = 'Grimzy\LaravelCrud\DataField';
            $validation = [];
            $field_config = [];

            $relation = ! empty($all_fields_config[$field_name]['relation']) ? $all_fields_config[$field_name]['relation'] : null;

            if (is_array($config)) {
                $class_name = ! empty($config['type']) ? $this->fields_namespace.'\\'.$config['type'] : $class_name;
                $validation = ! empty($config['validation']) ? $config['validation'] : [];
                $field_config = ! empty($config['config']) ? $config['config'] : [];
            }

            $this->fields[$field_name] = new $class_name($field_name, $this->resource, $this->model, $relation, $validation, $field_config);
        }
    }

    public function getResultsForAction($id = null)
    {
        if (! $this::$results) {
            $fields = $this->getFields();

            if (! empty($fields) && is_array($fields)) {
                $select = [];
                $with = [];
                foreach ($fields as $field_name => $field) {
                    if ($field->relation) {
                        if (method_exists($this->model, $field_name)) {
                            $with[] = $field_name;
                        }
                    } else {
                        $select[] = $field_name;
                    }
                }

                $query = $this->model::select($select);
                if (! empty($with)) {
                    $query->with($with);
                }

                if ($id) {
                    $this::$results = $query->find($id);
                } else {
                    if ($paginate = $this->config('controller.actions.'.$this->action.'.paginate')) {
                        $this::$results = $query->paginate($paginate);
                    } else {
                        $this::$results = $query->get();
                    }
                }
            }
        }

        return $this::$results;
    }

    public function syncRelations(&$item)
    {
        $all_fields_config = $this->getAllFieldsConfig();
        $fields_config = $this->getFieldsConfigForAction();

        foreach ($fields_config as $field_name => $field) {
            if (! empty($all_fields_config[$field_name]['relation'])) {
                if (method_exists($item, $field_name)) {
                    if ($related = $this->request->get($field_name)) {
                        $item->{$field_name}()->sync($related);
                    }
                }
            }
        }
    }

    public function update(&$item, CrudRequest $crud_request)
    {
        $this->syncRelations($item);
        // TODO: change the request to get all the processed fields
        // TODO: create a new ParameterBag and override $this->request->all() to return it
        $all = $crud_request->all();
        $item->update($all);
    }

    /*
     * Config stuff
     */

    public function config($key = null, $default = null)
    {
        return config($this->config_directory.'.'.$this->resource.'.'.$key, $default);
    }

    private function getAllFieldsConfig()
    {
        return $this->config('fields', []);
    }

    public function getFieldsConfigForAction()
    {
        $action = $this->action;
        $action_for = $this->config('controller.actions.'.$this->action.'.action_for', null);
        if(!empty($action_for)) {
            $action = $action_for;
        }

        return $this->config('controller.actions.'.$action.'.fields', []);
    }

    public function getAuth()
    {
        return $this->config('controller.auth');
    }

    public function getAuthForAction()
    {
        return $this->config('controller.actions.'.$this->action.'.auth');
    }

    /*
     * Getters & Setters
     */

    // TODO: Add the other getters

    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return \Grimzy\LaravelCrud\AbstractField[]
     */
    public function getFields()
    {
        return $this->fields;
    }
}