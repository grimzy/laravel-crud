<?php

namespace Grimzy\LaravelCrud;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CrudController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static $model;
    public static $resource;

    protected $crud;

    public function __construct(Crud $crud, Request $request)
    {
        $this->crud = $crud;

        if ($auth_middleware = $this->crud->getAuth()) {
            $this->middleware($auth_middleware);
        }

        if ($auth_action = $this->crud->getAuthForAction()) {
            $this->middleware($auth_action);
        }

    }

    public function index()
    {
        $items = $this->crud->getResultsForAction();

        $fields_config = $this->crud->getFieldsConfigForAction();
        $fields = array_keys(array_filter($fields_config));

        return view('crud.index')
            ->with('items', $items)
            ->with('fields', $fields)
            ->with('resource', $this::$resource)
            ->with('title', 'List: ' . ucfirst(trans('crud.resource.' . $this::$resource . '.name-plural')));
    }

    public function create()
    {
        $fields = array_keys(array_filter($this->crud->getFieldsConfigForAction()));

        return view('crud.create')
            ->with('resource', $this::$resource)
            ->with('fields', $fields)
            ->with('title', 'Create: ' . ucfirst(trans('crud.resource.' . $this::$resource . '.name')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CrudRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CrudRequest $request)
    {
        $item = $this::$model::create($request->all());

        $this->crud->syncRelations($item);

        $item->update();

        return redirect()->route($this::$resource . '.index')
            ->with('success', trans('crud.success.created', ['resource' => $this::$resource]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $item = $this->crud->getResultsForAction($id);

        if (!$item) {
            abort(404);
        }

        $fields = array_keys(array_filter($this->crud->getFieldsConfigForAction()));

        return view('crud.show', compact('item'))
            ->with('resource', $this::$resource)
            ->with('fields', $fields)
            ->with('title', 'Show: ' . ucfirst(trans('crud.resource.' . $this::$resource . '.name')) . ' (#' . $id . ')');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $item = $this->crud->getResultsForAction($id);

        if (!$item) {
            abort(404);
        }

        $fields = array_keys(array_filter($this->crud->getFieldsConfigForAction()));

        return view('crud.edit', compact('item'))
            ->with('resource', $this::$resource)
            ->with('fields', $fields)
            ->with('title', 'Edit: ' . ucfirst(trans('crud.resource.' . $this::$resource . '.name')) . ' (#' . $id . ')');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Grimzy\LaravelCrud\CrudRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CrudRequest $request, $id)
    {
        $item = $this->crud->getResultsForAction($id);

        if(!$item) {
            abort(404);
        }

        $this->crud->update($item, $request);

        return redirect()->route($this::$resource . '.show', [$id])
            ->with('success', trans('crud.success.updated', ['resource' => $this::$resource]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->crud->getResultsForAction($id)->delete();
        return redirect()->route($this::$resource . '.index')
            ->with('success', trans('crud.success.deleted', ['resource' => $this::$resource]));
    }
}