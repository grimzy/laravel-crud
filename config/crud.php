<?php

return [
    'config_directory' => 'crud',
    'views_directory' => 'crud',
    'fields_namespace' => 'App\Fields',
];